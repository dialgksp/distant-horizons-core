package com.seibel.distanthorizons.api.enums.config;

/**
 * STABLE, <br>
 * NIGHTLY, <br><br>
 *
 * @since API 1.1.0
 * @version 2024-4-6
 */
public enum EDhApiUpdateBranch
{
	STABLE,
	NIGHTLY
}
